import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:session_0/data/models/model_auth.dart';
import 'package:session_0/data/models/model_plaform.dart';
import 'package:session_0/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../models/model_profile.dart';

Future<List<dynamic>> get_new_with_coords() async {
  final data = await supabase
      .from('news')
      .select()
      .neq('geo_lat', 0).order('published_at', ascending: false);
  return data;
}

List<ModelPlatform> loadPlatforms() {
  List<dynamic> shortData = supabase.auth.currentUser!.userMetadata!["platforms"];

  List<ModelPlatform> platforms = shortData.map((e) =>
      ModelPlatform.fromJson(e)
  ).toList();

  return platforms;
}


Future<List<dynamic>> get_news(List<String> idPlatforms) async {
  final data = await supabase
      .from('news')
      .select()
      .inFilter('id_platform', idPlatforms)
      .order('published_at', ascending: false);
  return data;
}


Future<AuthResponse> signIn(
    ModelAuth modelAuth,
    ) async {
  return await supabase.auth.signInWithPassword(
      email: modelAuth.email,
      password: modelAuth.password
  );
}
Map<String, dynamic> get_profile_atributes() {
  return supabase.auth.currentUser!.userMetadata!;
}


Future<AuthResponse> signUp(
    ModelAuth modelAuth,
    ) async {
  return await supabase.auth.signUp(
      email: modelAuth.email,
      password: modelAuth.password,
    data: {
        "fullname": "Ivanov Ivan Ivanoch",
      "birthday": '',
      "phone": '',
      "avatar": null,
      "platforms": []
    }
  );
}




Future<void> log_out()async{
  return await supabase.auth.signOut();
}


Future<void> send_email(String email,)async{
    return await supabase.auth.resetPasswordForEmail(email);
}



Future<AuthResponse> verify_OTP(
      String email,
      String code
    )async{

    return await supabase.auth.verifyOTP(
      type: OtpType.email,
      token: code,
      email: email);
}


Future<UserResponse> change_pass(String pass)async{
    return await supabase.auth.updateUser(
      UserAttributes(
          password: pass
      ),
    );
}

Map<String, dynamic> getProfileAttributes(){
  return supabase.auth.currentUser!.userMetadata!;
}

Future<dynamic> getMyAvatar()async{
  if (supabase.auth.currentUser!.userMetadata!['avatar'] != null){
    var file = await supabase.storage.from('avatars').download(supabase.auth.currentUser!.userMetadata!['avatar']);
    return file;
  }
  return null;
}

Future<void> uploadAvatar(Uint8List bytes) async {
  var name = "${supabase.auth.currentUser!.id}.png";
  await supabase.storage
      .from("avatars")
      .uploadBinary(
      name,
      bytes
  );
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "avatar": name
          }
      )
  );
}

Future<void> removeAvatar() async {
  await supabase.storage
      .from("avatars")
      .remove(["${supabase.auth.currentUser!.id}.png"]);
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "avatar": null
          }
      )
  );
}

Future<void> update_user(ModelProfile profile
    )async{
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "fullname": profile.fullname,
            "phone": profile.phone,
            "birthday": profile.birthday,

          }
      )
  );

}
Future<Map<String, dynamic>> find_platform(String id)async{
  final data = await supabase
      .from('platforms')
      .select()
      .eq('id', id).single();
  return data;
}

void subsribeNews(callback){
  supabase
      .channel("changes")
      .onPostgresChanges(
      event: PostgresChangeEvent.insert,
      schema: "public",
      table: "news",
      callback: (payload) {
        callback(payload.newRecord);
      }
  )
      .subscribe();
}

