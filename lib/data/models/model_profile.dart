class ModelProfile {
  String fullname;
  var avatar;
  String phone;
  String birthday;


  ModelProfile(
      {required this.fullname,
      required this.phone,
      required this.birthday,
      this.avatar});
}
