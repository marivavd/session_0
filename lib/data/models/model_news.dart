import 'package:session_0/data/models/model_plaform.dart';

class ModelNews {
  final String id;
  final String title;
  final String text;
  final String media;
  final ModelPlatform platform;
  final String published;
  final String type_media;
  DateTime? favouriteTime;

  ModelNews({required this.id, required this.title, required this.text, required this.media, required this.published, required this.platform, required this.type_media, this.favouriteTime});
}
