import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:session_0/main.dart';

class CustomField extends StatefulWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final bool isValid;
  final Function(String)? onChange;

  const CustomField({super.key, required this.label, required this.hint, required this.controller, this.enableObscure = false,
    this.onChange,
    this.isValid = true
  });

  @override
  State<CustomField> createState() => _CustomFieldState();
}

class _CustomFieldState extends State<CustomField> {
  bool isObscure = true;


  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.subText),
        ),
        const SizedBox(height: 8,),
        SizedBox(
          height: 44,
          child: TextField(
            obscureText: (widget.enableObscure) ? isObscure : false,
            obscuringCharacter: '*',
            controller: widget.controller,
            style: Theme.of(context).textTheme.titleMedium?.copyWith(
              color: colors.text,
            ),
            onChanged: widget.onChange,
            decoration: InputDecoration(
              hintText: widget.hint,
              hintStyle:Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.hint),
              enabledBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.enabledBorder : Theme.of(context).inputDecorationTheme.errorBorder,
              focusedBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.focusedBorder : Theme.of(context).inputDecorationTheme.errorBorder,
              contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
              suffixIconConstraints: const BoxConstraints(minWidth: 34),
              suffixIcon: (widget.enableObscure)?
                  GestureDetector(
                    onTap: (){
                      setState(() {
                        isObscure = !isObscure;
                      });
                    },
                    child: SvgPicture.asset(
                        (!isObscure)
                            ? "assets/eye_open.svg"
                            : "assets/eye_close.svg",
                        // colorFilter: ColorFilter.mode(colors.iconTint, BlendMode.color)
                    ),
                  )
                  :null

            ),
          ),
        )
      ],
    );
  }

}
