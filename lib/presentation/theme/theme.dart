import 'package:flutter/material.dart';
import 'package:session_0/presentation/theme/colors.dart';

final colorsLights = LightColorsApp();
final light = ThemeData(
  inputDecorationTheme: InputDecorationTheme(
    helperStyle: TextStyle(
      color: colorsLights.disableAccent,
    ),
    hintStyle: TextStyle(
        color: colorsLights.hint
    ),
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.subText)
    ),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.subText)
    ),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.error)
    ),
  ),
  textTheme: TextTheme(
      titleMedium: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w500,
        color: colorsLights.subText
      ),
      titleLarge: TextStyle(
          fontSize: 24,
          fontWeight: FontWeight.w500,
          color: colorsLights.text
      ),
      labelLarge: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w700,
          color: colorsLights.disableTextAccent
      )
  ),
  filledButtonTheme: FilledButtonThemeData(
      style: FilledButton.styleFrom(
          textStyle: const TextStyle(fontWeight: FontWeight.bold),
          backgroundColor: colorsLights.accent,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
          disabledBackgroundColor: colorsLights.disableAccent,
          disabledForegroundColor: colorsLights.disableTextAccent
      )
  ),
);

final colorsDark = DarkColorsApp();
final dark = ThemeData(
  inputDecorationTheme: InputDecorationTheme(
    helperStyle: TextStyle(
      color: colorsLights.disableAccent,
    ),
    hintStyle: TextStyle(
        color: colorsLights.hint
    ),
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.subText)
    ),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.subText)
    ),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.error)
    ),
  ),
  textTheme: TextTheme(
      titleMedium: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w500,
        color: colorsDark.subText
      ),
      titleLarge: TextStyle(
          fontSize: 24,
          fontWeight: FontWeight.w500,
          color: colorsDark.text
      ),
      labelLarge: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w700,
          color: colorsDark.disableTextAccent
      )
  ),
  filledButtonTheme: FilledButtonThemeData(
      style: FilledButton.styleFrom(
          textStyle: const TextStyle(fontWeight: FontWeight.bold),
          backgroundColor: colorsLights.accent,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
          disabledBackgroundColor: colorsLights.disableAccent,
          disabledForegroundColor: colorsLights.disableTextAccent
      )
  ),
);
