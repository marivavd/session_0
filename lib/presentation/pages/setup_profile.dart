import 'dart:typed_data';

import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:session_0/data/models/model_profile.dart';
import 'package:session_0/domain/avatar_use_case.dart';
import 'package:session_0/domain/log_out_use_case.dart';
import 'package:session_0/domain/make_profile.dart';
import 'package:session_0/domain/update_user.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/profile.dart';
import 'package:session_0/presentation/pages/sign_in.dart';
import 'package:session_0/presentation/theme/theme.dart';
import 'package:session_0/presentation/utils.dart';
import 'package:session_0/presentation/widgets/text_field.dart';

class SetupProfile extends StatefulWidget {
  SetupProfile({super.key, required this.profile});
  ModelProfile profile;


  @override
  State<SetupProfile> createState() => _SetupProfileState();
}

class _SetupProfileState extends State<SetupProfile> {
  var fullname = TextEditingController();
  var phone = MaskedTextController(mask: "+7 (000) 000 00 00");
  var birthday = TextEditingController();
  late AvatarUseCase useCase;
  bool isValid = false;
  void onPickAvatar(Uint8List bytes){
    setState(() {
      widget.profile.avatar = bytes;
    });
  }

  void onRemoveAvatar(){
    setState(() {
      widget.profile.avatar = null;
    });
  }

  Future<ImageSource?> onChooseSource() async {

    ImageSource? imageSource;

    await showDialog(context: context, builder: (_) => AlertDialog(
      title: const Text("Выберите источник"),
      actions: [
        TextButton(onPressed: (){
          imageSource = ImageSource.camera;
          Navigator.pop(context);
        }, child: const Text("Камера")),
        TextButton(onPressed: (){
          imageSource = ImageSource.gallery;
          Navigator.pop(context);
        }, child: const Text("Галлерея"))
      ],
    ));

    return imageSource;
  }



  @override
  void initState() {
    super.initState();
    fullname.text = widget.profile.fullname;
    birthday.text = widget.profile.birthday;
    phone.text = widget.profile.phone;
    isValid = fullname.text.isNotEmpty && birthday.text.isNotEmpty && phone.text.isNotEmpty;
    setState(() {

    });
    useCase = AvatarUseCase(

      onChooseSource: onChooseSource,
      onPickAvatar: onPickAvatar,
      onRemoveAvatar: onRemoveAvatar,
    );
    setState(() {

    });

  }

  void onChange(_){
    setState(() {
      isValid = fullname.text.isNotEmpty && birthday.text.isNotEmpty && phone.text.isNotEmpty;
    });
  }




  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 73,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  OutlinedButton(
                    style: OutlinedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14),
                            side: BorderSide(
                                color: colors.accent,
                                width: 1
                            )
                        ),
                        minimumSize: Size.zero,
                        padding: const EdgeInsets.symmetric(
                            vertical: 12,
                            horizontal: 12
                        )
                    ),
                    onPressed: (){
                      useCase.pressButton((widget.profile.avatar != null) ? true : false);
                    },
                    child: Icon((widget.profile.avatar != null) ? Icons.restore_from_trash: Icons.add_photo_alternate_outlined, color: colors.accent,),),

                  ClipRRect(
                    borderRadius: BorderRadius.circular(32),
                    child: (widget.profile.avatar != null)?Container(
                      height: 151,
                      width: 151,
                      child: Image.memory(widget.profile.avatar, fit: BoxFit.cover,),

                    ): Image.asset("assets/empty.png", height: 151, width: 151,),
                  ),
                  OutlinedButton(
                    style: OutlinedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14),
                            side: BorderSide(
                                color: colors.accent,
                                width: 1
                            )
                        ),
                        minimumSize: Size.zero,
                        padding: const EdgeInsets.symmetric(
                            vertical: 12,
                            horizontal: 12
                        )
                    ),
                    onPressed: (){
                      MyApp.of(context).changeDiffTheme(context);
                    },
                    child: Icon((colors == colorsLights) ? Icons.sunny: Icons.dark_mode, color: colors.accent,),),

                ],
              ),
              SizedBox(height: 24,),
              CustomField(label: "ФИО", hint: "Введите ваше ФИО", controller: fullname, onChange: onChange,),
              SizedBox(height: 24,),
              CustomField(label: "Телефон", hint: "+7 (000) 000 00 00", controller: phone, onChange: onChange,),
              SizedBox(height: 24,),
              CustomField(label: "Дата рождения", hint: "Выберите дату", controller: birthday, onChange: onChange,),
              SizedBox(height: 100,),
              Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                      height: 46,
                      width: 342,
                      child: FilledButton(
                          child: Text(
                            "Сохранить",
                            style: Theme.of(context).textTheme.labelLarge,
                          ),
                        onPressed: (isValid) ? () async {
                          showLoading(context);
                          await prepare_to_update(profile: widget.profile, fullname: fullname.text, phone: phone.text, birthday: birthday.text, context: context);
                          hideLoading(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Profile(profile: widget.profile,))).then((value) => setState(() {}));
                        }:null,))),
              SizedBox(height: 18,),



              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  width: 342,
                  height: 46,
                  child: FilledButton(
                    style: FilledButton.styleFrom(
                        backgroundColor: Colors.transparent,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4),
                            side: BorderSide(
                                width: 1,
                                color: colors.accent
                            )
                        )
                    ),
                    onPressed: ()async{
                      showLoading(context);
                      widget.profile = await makeprofile();
                      hideLoading(context);
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Profile(profile: widget.profile))).then((value) => setState(() {}));
                    },
                    child: Text(
                      "Отменить",
                      style: Theme.of(context).textTheme.labelLarge!.copyWith(color: colors.accent),
                    ),

                  ),
                ),
              ),

            ],
          ),
        ),
      ),


    );
  }
}

