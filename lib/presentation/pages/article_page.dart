import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:session_0/data/models/model_news.dart';
import 'package:session_0/data/models/model_profile.dart';
import 'package:session_0/data/storage/favourite_news.dart';
import 'package:session_0/domain/is_favourite.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/media.dart';
import 'package:session_0/presentation/pages/setup_profile.dart';
import 'package:url_launcher/url_launcher.dart';

class ArticlePage extends StatefulWidget {
  ArticlePage({super.key, required this.news});
  ModelNews news;


  @override
  State<ArticlePage> createState() => _ArticlePageState();
}

class _ArticlePageState extends State<ArticlePage> {
  bool favourite = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    favourite = isFavourite(widget.news);
  }







  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 63,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      InkWell(
                        child: SvgPicture.asset('assets/back.svg', color: colors.iconTint, height: 16, width: 16,),
                        onTap: (){Navigator.of(context).pop();},
                      ),
                      SizedBox(width: 15,),
                      (widget.news.platform.icon != '')?Image.network(widget.news.platform.icon, height: 30, width: 30,):Container(height: 30, width: 30,),
                      SizedBox(width: 12,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(widget.news.platform.title,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: colors.text
                            ),
                          ),
                          Text(
                            widget.news.published,
                            style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w400,
                                color: colors.text
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                  Row(
                    children: [
                      SvgPicture.asset('assets/share.svg', color: colors.iconTint,),
                      SizedBox(width: 14,),
                      InkWell(
                        onTap: (){
                          setState(() {
                            (favourite)?
                            favouriteNews.remove(widget.news) :favouriteNews.insert(0, widget.news);
                            favourite = !favourite;

                          });
                        },
                        child: (favourite)?SvgPicture.asset('assets/yes_favourite.svg', color: colors.iconTint, key: Key('In favourite')):SvgPicture.asset('assets/no_favourite.svg', color: colors.iconTint, key: Key('Not in favourite'),),
                      )
                    ],
                  )

                ],
              ),
              SizedBox(height: 18,),
               Text(
                widget.news.title,
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    color: colors.text
                ),
              ),

              (widget.news.type_media == 'none')?
              SizedBox(height: 18,):(widget.news.type_media == 'link')?
              Column(
                children: [
                  InkWell(
                    child: Text(
                      widget.news.media,
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: colors.accent
                      ),
                    ),
                    onTap: (){
                      launchUrl(Uri.parse(widget.news.media));
                    },
                  ), SizedBox(height: 18,)
                ],
              ):(widget.news.type_media == 'photo')?
              Column(
                children: [
                  InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Media(news: widget.news))).then((value) => setState(() {

                    }));
                  },
                    child: Image.network(widget.news.media),),
                  SizedBox(height: 18,)
                ],

              ): Column(
                children: [
                  Image.asset('assets/audio.png'),
                  SizedBox(height: 18,)
                ],
              ),
              Text(
                widget.news.text,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colors.text
                ),
              )
            ],
          ),
        ),
      ),


    );
  }
}

