

import 'dart:async';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:session_0/data/models/map_point.dart';
import 'package:session_0/data/models/model_news.dart';
import 'package:session_0/domain/accelerometr_use_case.dart';
import 'package:session_0/domain/get_map_points.dart';
import 'package:session_0/domain/make_card_news.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/cluster.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class MapPage extends StatefulWidget {
  const MapPage({super.key});

  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  Completer<YandexMapController> _completer = Completer();
  List<ClusterizedPlacemarkCollection> cluster = [];
  List<ModelNews> news = [];
  CarouselController buttonCarouselController = CarouselController();
  int index = 0;
  List<MapPoint> data = [];
  AccelerometerUseCase useCase = AccelerometerUseCase();
  double x = 0, y = 0, z = 0;
  String? error;
  bool isLoading = true;


  Future<ClusterizedPlacemarkCollection> createCluster(String id)async{
    List<PlacemarkMapObject> placemarkMap = [];
    for (int i = 0; i < news.length; i ++){
      if (news[i].id == id){
        buttonCarouselController.jumpToPage(i);

      }
    }
    for (int i =0; i<data.length; i++){
      if (data[i].name == id){
        placemarkMap.add(PlacemarkMapObject(
            mapId: MapObjectId(data[i].name),
            point: Point(latitude: data[i].latitude, longitude: data[i].longitude),
            opacity: 1,
            icon: PlacemarkIcon.single(
                PlacemarkIconStyle(
                    image: BitmapDescriptor.fromAssetImage(
                        "assets/red.png"
                    ),
                    scale: 2
                )
            ),
            onTap: (_, a)async{
              final cluster_old = await createCluster(data[i].name);
              setState(() {
                cluster = [cluster_old];

              });
            }
              )
              );
      }

    }
    bool flag = false;
    for (int i =0; i<data.length; i++){
      if (data[i].name == id){
        flag = true;
      }
      else{
        placemarkMap.add(PlacemarkMapObject(
            mapId: MapObjectId(data[i].name),
            point: Point(latitude: data[i].latitude, longitude: data[i].longitude),
            opacity: 1,
            icon: PlacemarkIcon.single(
                PlacemarkIconStyle(
                    image: BitmapDescriptor.fromAssetImage(
                        (flag)? "assets/green.png":"assets/blue.png"
                    ),
                    scale: 2
                )
            ),
            onTap: (_, a)async{
              final cluster_old = await createCluster(data[i].name);
              setState(() {
                cluster = [cluster_old];

              });
            }
        ));
      }
    }
    final cluster_1 = ClusterizedPlacemarkCollection(
        mapId: MapObjectId("cluster"),
        placemarks: placemarkMap,

        radius: 30,
        minZoom: 15,
        isVisible: true,
        onClusterAdded: (self, cluster1)async{
          return cluster1.copyWith(
            appearance: cluster1.appearance.copyWith(
              opacity: 1.0,
              icon: PlacemarkIcon.single(
                PlacemarkIconStyle(
                  image: BitmapDescriptor.fromBytes(
                    await ClusterIconPainter(cluster1.size).getClusterIconBytes(),
                  ),
                ),
              ),
            ),
          );
        }
    );
    return cluster_1;

  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      final data_old =  await gatMapPoints();
      data = data_old;
      final news_old = await makeCardNews();
      final cluster_old = await createCluster(news_old[0].id);
      cluster = [cluster_old];
      news = news_old;
      setState(() {

      });
    });
    useCase.launchListenSensor(
        onChange: (event){

          setState(() {
            if (isLoading) {
              isLoading = false;
            }
            if (x > event.x && x - event.x > 0.5){
              buttonCarouselController.nextPage();
            }
            x = event.x;
            y = event.y;
            z = event.z;
          });
        },
        onError: (error) {
          setState(() {
            this.error = error.toString();
          });
        }
    );


  }
  @override
  void dispose() {
    super.dispose();
    useCase.dispose();
  }





  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
      body: (news.length != 0)?
        CustomScrollView(
            slivers: [SliverToBoxAdapter(
                child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 77,),

            SizedBox(
              height: 449,
              child: YandexMap(
                zoomGesturesEnabled: true,
                onMapCreated: _onMapCreated,
                mapObjects: cluster,
              ),
            ),
            SizedBox(height: 32,),])),
              SliverToBoxAdapter(
              child: SizedBox(
                child: CarouselSlider.builder(
                  carouselController: buttonCarouselController,

                  options: CarouselOptions(
                    height: 255,
                    autoPlay: false,
                    onPageChanged: (index, f)async{
                       buttonCarouselController.jumpToPage(index);
                        final cluster_old = await createCluster(news[index].id);
                        setState(() {
                          cluster = [cluster_old];
                        });
                    },

                  ),
                  itemCount: news.length,
                  itemBuilder: (context, itemIndex, realIndex)
                  {
                    index = itemIndex;
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 22),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 12,),
                          Row(
                            children: [
                              Image.network(news[itemIndex].platform.icon, height: 30, width: 30,),
                              SizedBox(width: 12,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(news[itemIndex].platform.title,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        color: colors.text
                                    ),
                                  ),
                                  Text(
                                    news[itemIndex].published,
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400,
                                        color: colors.text
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                          SizedBox(height: 12,),
                          Flexible(
                              child: Text(
                                (news[itemIndex].title.length > 250)?
                                "${news[itemIndex].title.substring(0, 250)}..."
                                    :news[itemIndex].title,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    color: colors.text
                                ),
                              )
                          ),
                          SizedBox(height: 12,),
                          Flexible(
                              child: Text(
                                (news[itemIndex].text.length > 500)?
                                "${news[itemIndex].text.substring(0, 500)}..."
                                    :news[itemIndex].text,
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: colors.text
                                ),
                              )
                          ),
                          SizedBox(height: 32,)

                        ],
                      ),
                    );
                  },
                ),
              )

            ),
              //SliverToBoxAdapter(child: SizedBox(height: 32,),)

          ],

      ):Center(child: CircularProgressIndicator(),)
    );
  }
  Future<void> _onMapCreated(YandexMapController controller)async{

    _completer.complete(controller);


  }




}
