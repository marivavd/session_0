import 'package:flutter/material.dart';
import 'package:session_0/domain/controllers/password_controller.dart';
import 'package:session_0/domain/sign_in_use_case.dart';
import 'package:session_0/domain/sign_up_use_case.dart';
import 'package:session_0/domain/utils.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/forgot_password.dart';
import 'package:session_0/presentation/pages/home.dart';
import 'package:session_0/presentation/pages/sign_up.dart';
import 'package:session_0/presentation/utils.dart';
import 'package:session_0/presentation/widgets/text_field.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _MapPageState();
}

class _MapPageState extends State<SignIn> {
  TextEditingController email = TextEditingController(text: "mar@gmail.com");
  var password = PasswordController();
  bool rememberPassword = false;

  SignInUseCase useCase = SignInUseCase();

  bool isRedEmail = false;
  bool isValid = false;


  void onChange(_){
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail && password.text.isNotEmpty;
    });
  }






  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
        backgroundColor: colors.background,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 83,),
                Text(
                  "Добро пожаловать",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(height: 8,),
                Text("Заполните почту и пароль чтобы продолжить",
                  style: Theme.of(context).textTheme.titleMedium,),
                SizedBox(height: 28,),
                CustomField(label: "Почта", hint: "***********@mail.com", controller: email, isValid: !isRedEmail, onChange: onChange,),
                SizedBox(height: 24,),
                CustomField(label: "Пароль", hint: "**********", controller: password, enableObscure: true, onChange: onChange,),
                SizedBox(height: 18,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          height: 22,
                          width: 22,
                          child: Checkbox(
                            value: rememberPassword,
                            onChanged: (value){
                              setState(() {
                                rememberPassword = value!;
                              });
                            },
                            side: BorderSide(
                              width: 1,
                              color: colors.subText,

                            ),
                            activeColor: colors.accent,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            ),
                          ),
                        ),
                        SizedBox(width: 8,),
                        Text(
                          "Запомнить меня",
                          style: Theme.of(context).textTheme.titleMedium,
                        ),

                      ],
                    ),
                    InkWell(
                      onTap: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => ForgotPass())).then((value) => setState(() {
                      }));},
                      child: Text('Забыли пароль?',style: Theme.of(context).textTheme.titleMedium!.copyWith(color: colors.accent,
                      )
                      ),
                    )
                  ],
                ),
                SizedBox(height: 371,),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 46,
                    width: 342,
                    child: FilledButton(
                      onPressed: (isValid) ? (){
                        showLoading(context);
                        useCase.pressButtonSignIn(email.text,
                            password.hash_pass(),
                                (_){
                              hideLoading(context);
                              Navigator.push(context, MaterialPageRoute(builder: (context) => Home())).then((value) => setState(() {

                              }));

                            },
                                (error) async {
                              hideLoading(context);
                              await showError(context, error);
                            }

                        );
                      }:null,
                      child: Text(
                        "Войти",
                        style: Theme.of(context).textTheme.labelLarge,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 14,),
                Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp())).then((value) => setState(() {

                      }));
                    },
                    child: RichText(text: TextSpan(
                        children: [
                          TextSpan(
                              text: "У меня нет аккаунта! ",
                              style: TextStyle(fontSize: 14, color:colors.subText)
                          ),
                          TextSpan(
                              text: "Cоздать",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: colors.accent,
                                  fontWeight: FontWeight.w700
                              )
                          ),
                        ]
                    )),
                  ),
                ),
                const SizedBox(height: 34),


              ],
            ),
          ),
        )
    );
  }



}
