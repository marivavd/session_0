import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:session_0/data/models/model_news.dart';
import 'package:session_0/data/models/model_profile.dart';
import 'package:session_0/data/repository/requests.dart';
import 'package:session_0/data/storage/favourite_news.dart';
import 'package:session_0/domain/get_time_for_favourites.dart';
import 'package:session_0/domain/make_card_news.dart';
import 'package:session_0/domain/queue.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/article_page.dart';
import 'package:session_0/presentation/pages/setup_profile.dart';

class Favourite extends StatefulWidget {
  Favourite({super.key});


  @override
  State<Favourite> createState() => _FavouriteState();
}

class _FavouriteState extends State<Favourite> {
  List<ModelNews> news = getListNews();
  bool flag = false;


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {

      setState(() {


        flag = true;
      });

    });
  }


  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    var width = MediaQuery.of(context).size.width;
    return (flag)?Scaffold(
      backgroundColor: colors.background,
      body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
                child:  Padding(
                  padding: EdgeInsets.symmetric(horizontal: 22),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 63,),
                      Text(
                        'Избранное',
                            style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                color: colors.text,
                                height: 16/24
                            ),
                          ),
                      SizedBox(height: 18,),

                    ],
                  ),
                )

            ),
            SliverList.builder(
                itemCount: news.length,
                itemBuilder: (_, index){
                  return Padding(
                      padding: EdgeInsets.symmetric(horizontal: 22),
                      child: Column(
                        children: [
                          SizedBox(height: 12,),
                          Row(
                            children: [
                              Expanded(child:
                              Row(
                                children: [
                                  (news[index].platform.icon != '')?Image.network(news[index].platform.icon, height: 30, width: 30,):Container(height: 30, width: 30,),
                                  SizedBox(width: 12,),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(news[index].platform.title,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w400,
                                                color: colors.text
                                            ),
                                          ),
                                          SvgPicture.asset('assets/point.svg', color: colors.text,),
                                          Text(news[index].platform.channels,
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                color: colors.text,
                                                height: 16/12
                                            ),
                                          ),
                                        ],
                                      ),

                                      Text(
                                        news[index].published,
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w400,
                                            color: colors.text,
                                            height: 16/10
                                        ),
                                      )
                                ],
                              )
                                ],
                              )),
                              Row(
                                children: [
                                  Text(
                                    getTimeForFavourites(news[index].favouriteTime!),
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w600,
                                        color: colors.hint,
                                        height: 16/10
                                    ),
                                  ),
                                  SizedBox(width: 12,),
                                  InkWell(
                                    child: SvgPicture.asset('assets/close.svg', color: colors.iconTint, width: 14, height: 14,),
                                    onTap: (){
                                      setState(() {
                                        remove(news[index]);
                                      });

                                    },
                                      key: Key("Delete")

                                  )

                                ],
                              )

                            ],
                          ),
                          SizedBox(height: 12,),
                          InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news[index]))).then((value) => setState(() {

                                }));
                              },
                              child: Text(
                                (news[index].title.length > 500)?
                                "${news[index].title.substring(0, 500)}..."
                                    :news[index].title,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  color: colors.text,
                                  height: 64/54

                                ),

                              )),
                          SizedBox(height: 8,),
                          InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news[index]))).then((value) => setState(() {

                                }));
                              },
                              child: Text(
                                (news[index].text.length > 1550)?
                                "${news[index].text.substring(0, 1500)}..."
                                    :news[index].text,
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: colors.text,
                                    height: 84/72
                                ),

                              )),
                          SizedBox(height: 24,),
                          Divider(height: 1, color: colors.text,),
                          SizedBox(height: 13,)
                        ],
                      )
                  );
                })


          ]
      ),


    ):Center(child: CircularProgressIndicator(),);
  }
}

