import 'package:flutter/material.dart';
import 'package:session_0/domain/change_pass_use_case.dart';
import 'package:session_0/domain/controllers/password_controller.dart';
import 'package:session_0/domain/sign_up_use_case.dart';
import 'package:session_0/domain/utils.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/sign_in.dart';
import 'package:session_0/presentation/utils.dart';
import 'package:session_0/presentation/widgets/text_field.dart';

class ChangePass extends StatefulWidget {
  const ChangePass({super.key});

  @override
  State<ChangePass> createState() => _MapPageState();
}

class _MapPageState extends State<ChangePass> {
  var password = PasswordController();
  var confirmpassword = PasswordController();

  ChangePassUseCase useCase = ChangePassUseCase();

  bool isValid = false;


  void onChange(_){
    setState(() {
      isValid = password.text.isNotEmpty && confirmpassword.text.isNotEmpty;
    });
  }






  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
        backgroundColor: colors.background,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 83,),
                Text(
                  "Новый пароль",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(height: 8,),
                Text("Введите новый пароль",
                  style: Theme.of(context).textTheme.titleMedium,),
                SizedBox(height: 28,),
                CustomField(label: "Пароль", hint: "**********", controller: password,enableObscure: true, onChange: onChange,),
                SizedBox(height: 24,),
                CustomField(label: "Повторите пароль", hint: "**********", controller: confirmpassword, enableObscure: true, onChange: onChange,),
                SizedBox(height: 411,),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 46,
                    width: 342,
                    child: FilledButton(
                      onPressed: (isValid) ? (){
                        showLoading(context);
                        useCase.pressButtonChangePass(
                            password.hash_pass(),
                            confirmpassword.hash_pass(),
                                (_){
                              hideLoading(context);
                              Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                              }));

                            },
                                (error) async {
                              hideLoading(context);
                              await showError(context, error);
                            }

                        );
                      }:null,
                      child: Text(
                        "Подтвердить",
                        style: Theme.of(context).textTheme.labelLarge,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 14,),
                Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                      }));
                    },
                    child: RichText(text: TextSpan(
                        children: [
                          TextSpan(
                              text: "Я вспомнил свой пароль!",
                              style: TextStyle(fontSize: 14, color:colors.subText)
                          ),
                          TextSpan(
                              text: "Вернуться",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: colors.accent,
                                  fontWeight: FontWeight.w700
                              )
                          ),
                        ]
                    )),
                  ),
                ),
                const SizedBox(height: 34),


              ],
            ),
          ),
        )
    );
  }



}
