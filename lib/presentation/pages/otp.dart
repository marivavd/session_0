import 'package:flutter/material.dart';
import 'package:session_0/domain/controllers/password_controller.dart';
import 'package:session_0/domain/forgot_use_case.dart';
import 'package:session_0/domain/otp_use_case.dart';
import 'package:session_0/domain/sign_in_use_case.dart';
import 'package:session_0/domain/sign_up_use_case.dart';
import 'package:session_0/domain/utils.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/change_pass.dart';
import 'package:session_0/presentation/pages/home.dart';
import 'package:session_0/presentation/pages/sign_in.dart';
import 'package:session_0/presentation/pages/sign_up.dart';
import 'package:session_0/presentation/utils.dart';
import 'package:session_0/presentation/widgets/text_field.dart';

class OTP extends StatefulWidget {
  OTP({super.key, required this.email});
  String email;

  @override
  State<OTP> createState() => _MapPageState();
}

class _MapPageState extends State<OTP> {
  TextEditingController controller = TextEditingController();

 OTPUseCase useCase = OTPUseCase();
 ForgotUseCase useCaseRepeat = ForgotUseCase();

  bool isValid = false;


  void onChange(_){
    setState(() {

      isValid = controller.text.length == 6;
    });
  }






  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
      backgroundColor: colors.background,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 83,),
                Text(
                  "Верификация",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(height: 8,),
                Text("Введите 6-ти значный код из письма ",
                  style: Theme.of(context).textTheme.titleMedium,),
                SizedBox(height: 28,),
                CustomField(label: "", hint: "", controller: controller, onChange: onChange,),
                SizedBox(height: 48,),
                Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                    onTap: ()async{
                      showLoading(context);
                      useCaseRepeat.pressButtonForgot(widget.email,
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => OTP(email: widget.email))).then((value) => setState(() {

                            }));

                          },
                              (error) async {
                            hideLoading(context);
                            await showError(context, error);
                          }

                      );
                    },
                    child: Text("Получить новый код",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: colors.accent,
                                  fontWeight: FontWeight.w700
                              )

                    ),
                  ),
                ),
                SizedBox(height: 449,),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 46,
                    width: 342,
                    child: FilledButton(
                      onPressed: (isValid) ? (){
                        showLoading(context);
                        useCase.pressButtonOTP(widget.email,
                            controller.text,
                                (_){
                              hideLoading(context);
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePass())).then((value) => setState(() {

                              }));

                            },
                                (error) async {
                              hideLoading(context);
                              await showError(context, error);
                            }

                        );
                      }:null,
                      child: Text(
                        "Сбросить пароль",
                        style: Theme.of(context).textTheme.labelLarge,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 14,),
                Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                      }));
                    },
                    child: RichText(text: TextSpan(
                        children: [
                          TextSpan(
                              text: "Я вспомнил свой пароль!",
                              style: TextStyle(fontSize: 14, color:colors.subText)
                          ),
                          TextSpan(
                              text: "Вернуться",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: colors.accent,
                                  fontWeight: FontWeight.w700
                              )
                          ),
                        ]
                    )),
                  ),
                ),
                const SizedBox(height: 34),


              ],
            ),
          ),
        )
    );
  }



}
