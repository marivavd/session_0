import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:session_0/data/models/model_news.dart';
import 'package:session_0/data/models/model_profile.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/setup_profile.dart';

class Media extends StatefulWidget {
  Media({super.key, required this.news});
  ModelNews news;


  @override
  State<Media> createState() => _MediaState();
}

class _MediaState extends State<Media> {







  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);


    return Scaffold(
      backgroundColor: colors.background,
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 22),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 57,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            InkWell(
                              child: SvgPicture.asset('assets/close.svg', color: colors.iconTint, height: 18, width: 18,),
                              onTap: (){Navigator.of(context).pop();},
                            ),
                            SizedBox(width: 15,),
                            Image.network(widget.news.platform.icon, height: 30, width: 30,),
                            SizedBox(width: 12,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(widget.news.platform.title,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),
                                ),
                                Text(
                                  widget.news.published,
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        Row(
                          children: [
                            SvgPicture.asset('assets/share.svg', color: colors.iconTint,),
                            SizedBox(width: 14,),
                            SvgPicture.asset('assets/choosen.svg', color: colors.iconTint,),
                          ],
                        )

                      ],
                    ),
                    SizedBox(height: 22,),
                    Text(
                      widget.news.title,
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: colors.iconTint
                      ),
                    )])),),
          Expanded(child: Image.network(widget.news.media,
            width: double.infinity,),),
          Expanded(child: Padding(
            padding: EdgeInsets.all(22),
            child: Text(
              widget.news.text,
              maxLines: 5,
              style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: colors.subText
              ),

            ),
          ))
              

            ],
          ),


    );
  }
}
