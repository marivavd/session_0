import 'package:flutter/material.dart';
import 'package:session_0/domain/controllers/password_controller.dart';
import 'package:session_0/domain/sign_up_use_case.dart';
import 'package:session_0/domain/utils.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/sign_in.dart';
import 'package:session_0/presentation/utils.dart';
import 'package:session_0/presentation/widgets/text_field.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _MapPageState();
}

class _MapPageState extends State<SignUp> {
  TextEditingController email = TextEditingController();
  var password = PasswordController();
  var confirmpassword = PasswordController();

  SignUpUseCase useCase = SignUpUseCase();

  bool isRedEmail = false;
  bool isValid = false;


  void onChange(_){
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail && password.text.isNotEmpty && confirmpassword.text.isNotEmpty;
    });
  }






  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
        backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                "Создать аккаунт",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text("Завершите регистрацию чтобы начать",
              style: Theme.of(context).textTheme.titleMedium,),
              SizedBox(height: 28,),
              CustomField(label: "Почта", hint: "***********@mail.com", controller: email, isValid: !isRedEmail, onChange: onChange,),
              SizedBox(height: 24,),
              CustomField(label: "Пароль", hint: "**********", controller: password,enableObscure: true, onChange: onChange,),
              SizedBox(height: 24,),
              CustomField(label: "Повторите пароль", hint: "**********", controller: confirmpassword, enableObscure: true, onChange: onChange,),

              SizedBox(height: 319,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    onPressed: (isValid) ? (){
                      showLoading(context);
                      useCase.pressButtonSignUp(email.text,
                          password.hash_pass(),
                          confirmpassword.hash_pass(),
                          (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                            }));

                          },
                              (error) async {
                            hideLoading(context);
                            await showError(context, error);
                          }

                      );
                    }:null,
                    child: Text(
                      "Зарегистрироваться",
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Align(
                alignment: Alignment.center,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                    }));
                  },
                  child: RichText(text: TextSpan(
                      children: [
                        TextSpan(
                            text: "У меня уже есть аккаунт! ",
                            style: TextStyle(fontSize: 14, color:colors.subText)
                        ),
                        TextSpan(
                            text: "Войти",
                            style: TextStyle(
                                fontSize: 14,
                                color: colors.accent,
                                fontWeight: FontWeight.w700
                            )
                        ),
                      ]
                  )),
                ),
              ),
              const SizedBox(height: 34),


            ],
          ),
        ),
      )
    );
  }



}
