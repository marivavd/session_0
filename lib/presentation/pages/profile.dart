import 'package:flutter/material.dart';
import 'package:session_0/data/models/model_profile.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/setup_profile.dart';

class Profile extends StatefulWidget {
  Profile({super.key, required this.profile});
  ModelProfile profile;


  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {







  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 73,),
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(32),
                    child: (widget.profile.avatar != null)?Container(
                      height: 121,
                      width: 121,
                      child: Image.memory(widget.profile.avatar, fit: BoxFit.cover,),

                    ): Image.asset("assets/empty.png", height: 121, width: 121,),
                  ),
                  const SizedBox(width: 24,),
                  Flexible(child: Text(
                    widget.profile.fullname,
                    overflow: TextOverflow.fade,
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: colors.text
                    ),
                  ),)
                ],
              ),
              SizedBox(height: 28,),
              Divider(height: 1, color: colors.subText,),
              SizedBox(height: 25,),
              Text(
                "История",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: colors.text
                ),
              ),
              SizedBox(height: 18,),
              Text(
                "Прочитанные статьи",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colors.text
                ),
              ),
              SizedBox(height: 18,),
              Text(
                "Чёрный список",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colors.text
                ),
              ),
              SizedBox(height: 24,),
              Text(
                "Настройки",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: colors.text
                ),
              ),
              SizedBox(height: 18,),
              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => SetupProfile(profile: widget.profile))).then((value) => setState(() {

                  }));
                },
                child: Text(
                  "Редактирование профиля",
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: colors.text
                  ),
                ),
              ),
              SizedBox(height: 18,),
              Text(
                "Политика конфиденциальности",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colors.text
                ),
              ),
              SizedBox(height: 18,),
              Text(
                "Оффлайн чтение",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colors.text
                ),
              ),
              SizedBox(height: 18,),
              Text(
                "О нас",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colors.text
                ),
              ),
            ],
          ),
        ),
      ),


    );
  }
}

