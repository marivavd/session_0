import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:session_0/data/models/model_news.dart';
import 'package:session_0/data/models/model_profile.dart';
import 'package:session_0/data/repository/requests.dart';
import 'package:session_0/domain/make_card_news.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/article_page.dart';
import 'package:session_0/presentation/pages/setup_profile.dart';

class HomePage extends StatefulWidget {
  HomePage({super.key, required this.profile});
  ModelProfile profile;


  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int indexNews = 0;
  List<ModelNews> news = [];
  List<ModelNews> news_media = [];
  bool flag = false;


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      final newsold = await makeNewsHome();

      setState(() {
        news = newsold;

        for (int i = 0; i < news.length; i ++){
          if (news[i].type_media != 'none'){
            news_media.add(news[i]);
          }
        }
        flag = true;
      });
    });
    subsribeNews((modelNews)=>{
      setState(() {
        WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
          final newsold = await makeNewsHome();

          setState(() {
            news = newsold;
            news_media.clear();
            for (int i = 0; i < news.length; i ++){
              if (news[i].type_media != 'none'){
                news_media.add(news[i]);
              }
            }
          });
        });
      })
    });

  }

  void changeLayout(int newIndex){
    setState(() {
      indexNews = newIndex;
    });
    Navigator.pop(context);
    showModelDialog();
  }

  void showModelDialog(){
    var colors = MyApp.of(context).getColors(context);
    showDialog(context: context, builder: (_) => Dialog(
      surfaceTintColor: Colors.transparent,
      insetPadding: const EdgeInsets.symmetric(horizontal: 22),
      backgroundColor: colors.block,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12))
      ),
      child: SizedBox(
        width: double.infinity,
        height: 248,
        child: Padding(
          padding: const EdgeInsets.only(top: 18, bottom: 28, left: 22, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("СЕТКА ОТОБРАЖЕНИЯ", style: TextStyle(
                color: colors.subText,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              )),
              const SizedBox(height: 16),
              Divider(height: 1, color: colors.subText, endIndent: 2),
              const SizedBox(height: 28),
              GestureDetector(
                onTap: (){
                  changeLayout(0);
                },
                child: Row(
                  children: [
                    SvgPicture.asset("assets/model0${(indexNews == 0) ? "check" : ""}.svg"),
                    const SizedBox(width: 18),
                    Expanded(
                      child: Text("Card", style: TextStyle(
                          color: colors.text,
                          fontSize: 18,
                          fontWeight: (indexNews == 0) ? FontWeight.bold : FontWeight.normal
                      )),
                    ),
                    (indexNews == 0) ? SvgPicture.asset("assets/check.svg", color: colors.iconTint,) : const SizedBox(),
                    const SizedBox(width: 22),
                  ],
                ),
              ),
              const SizedBox(height: 28),
              GestureDetector(
                onTap: (){
                  changeLayout(1);
                },
                child: Row(
                  children: [
                    SvgPicture.asset("assets/model1${(indexNews == 1) ? "check" : ""}.svg"),
                    const SizedBox(width: 18),
                    Expanded(
                      child: Text("Text", style: TextStyle(
                          color: colors.text,
                          fontSize: 18,
                          fontWeight: (indexNews == 1) ? FontWeight.bold : FontWeight.normal
                      )),
                    ),
                    (indexNews == 1) ? SvgPicture.asset("assets/check.svg", color: colors.iconTint,) : const SizedBox(),
                    const SizedBox(width: 22),
                  ],
                ),
              ),
              const SizedBox(height: 28),
              GestureDetector(
                onTap: (){
                  changeLayout(2);
                },
                child: Row(
                  children: [
                    SvgPicture.asset("assets/model2${(indexNews == 2) ? "check" : ""}.svg"),
                    const SizedBox(width: 18),
                    Expanded(
                      child: Text("Media", style: TextStyle(
                          color: colors.text,
                          fontSize: 18,
                          fontWeight: (indexNews == 2) ? FontWeight.bold : FontWeight.normal
                      )),
                    ),
                    (indexNews == 2) ? SvgPicture.asset("assets/check.svg") : const SizedBox(),
                    const SizedBox(width: 22),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }












  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    var width = MediaQuery.of(context).size.width;
    return (flag)?Scaffold(
      backgroundColor: colors.background,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
              child:  Padding(
                padding: EdgeInsets.symmetric(horizontal: (indexNews == 2)?4: 22),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 63,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(width: (indexNews == 2)?18: 0,),
                        Expanded(child: Text(
                          'Лента новостей',
                          style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                              color: colors.text
                          ),
                        ),),
                        InkWell(
                          onTap: (){
                            showModelDialog();
                          },
                          child: SvgPicture.asset('assets/model$indexNews.svg', color: colors.iconTint, height: 22, width: 22,),
                        ),
                        SizedBox(width: (indexNews == 2)?18: 0,),
                      ],
                    ),
                    SizedBox(height: 18,),

                  ],
                ),
              )

          ),
          SliverList.builder(
              itemCount: (indexNews == 2)?news_media.length:news.length,
              itemBuilder: (_, index){
                return Padding(
                    padding: EdgeInsets.symmetric(horizontal: (indexNews == 2)?4: 22),
                    child: (indexNews != 2)?Column(
                      children: [
                        SizedBox(height: 12,),
                        Row(
                          children: [
                            Image.network(news[index].platform.icon, height: 30, width: 30,),
                            SizedBox(width: 12,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(news[index].platform.title,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),
                                ),
                                Text(
                                  news[index].published,
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        SizedBox(height: 12,),
                        (indexNews == 0 && news[index].type_media != 'none')?

                        InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news[index]))).then((value) => setState(() {

                            }));
                          },
                        child: SizedBox(
                          width: width - 48,
                          child: Row(

                            children: [
                              Expanded(child: Text(
                                (news[index].title.length > 250)?
                                "${news[index].title.substring(0, 250)}..."
                                    :news[index].title,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    color: colors.text
                                ),
                              )),
                              (news[index].type_media == 'photo')?
                              Expanded(child: Image.network(news[index].media, width: 100, height: 75,)):
                              Expanded(child: Container(color: colors.block, width: 100, height: 75))
                            ],
                          ),
                        )):InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news[index]))).then((value) => setState(() {

                            }));
                          },
                            child: Text(
                                (news[index].title.length > 250)?
                                "${news[index].title.substring(0, 250)}..."
                                    :news[index].title,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    color: colors.text
                                ),

                        )),
                        SizedBox(height: 12,),
                       InkWell(
                         onTap: (){
                           Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news[index]))).then((value) => setState(() {

                           }));
                         },
                       child: Text(
                                  (news[index].text.length > 500)?
                                  "${news[index].text.substring(0, 500)}..."
                                      :news[index].text,
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),

                        )),
                        SizedBox(height: 12,),
                        Divider(height: 1, color: colors.text,),
                        SizedBox(height: 12,)
                      ],
                    ):
                    (news_media[index].type_media == 'photo')?
                        InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news_media[index]))).then((value) => setState(() {

                            }));
                          },
                        child: Image.network(news_media[index].media, width: (width - 12) / 2,)):
                        InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news_media[index]))).then((value) => setState(() {

                            }));
                          },
                            child: Container(color: colors.block, width: (width - 12) / 2))
                );
              })


        ]
      ),


    ):Center(child: CircularProgressIndicator(),);
  }
}

