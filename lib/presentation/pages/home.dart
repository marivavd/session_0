import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:session_0/data/models/model_profile.dart';
import 'package:session_0/domain/log_out_use_case.dart';
import 'package:session_0/domain/make_profile.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/favourite.dart';
import 'package:session_0/presentation/pages/home_page.dart';
import 'package:session_0/presentation/pages/map.dart';
import 'package:session_0/presentation/pages/profile.dart';
import 'package:session_0/presentation/pages/sign_in.dart';
import 'package:session_0/presentation/utils.dart';

class Home extends StatefulWidget {
  const Home({super.key});


  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ModelProfile? profile;
  int index = 0;


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      profile = await makeprofile();
      setState(() {

      });
    });

  }




  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
      backgroundColor: colors.background,
      body: (profile != null)?[HomePage(profile: profile!), MapPage(), Favourite(), Profile(profile: profile!),][index]:Center(child:CircularProgressIndicator(),),
    bottomNavigationBar: BottomNavigationBar(
    currentIndex: index,
    onTap: (val){
      setState(() {
        index = val;
      });
    },
    type: BottomNavigationBarType.fixed,
    selectedLabelStyle: TextStyle(
    color: colors.accent,
    fontWeight: FontWeight.w400,
    fontSize: 12
    ),
    unselectedLabelStyle: TextStyle(
    color: colors.text,
    fontWeight: FontWeight.w400,
    fontSize: 12
    ),
    showSelectedLabels: true,
    showUnselectedLabels: true,
    selectedFontSize: 12,
    selectedItemColor: colors.accent,
    unselectedFontSize: 12,
    iconSize: 24,
    unselectedItemColor: colors.text,
    items: [
      BottomNavigationBarItem(icon: SvgPicture.asset("assets/home.svg", color: (index == 0)? colors.accent:colors.text), label: "Home"),
    BottomNavigationBarItem(icon: SvgPicture.asset("assets/search.svg", color: (index == 1)? colors.accent:colors.text), label: "Search"),
    BottomNavigationBarItem(icon: SvgPicture.asset("assets/favourite.svg", color: (index == 2)? colors.accent:colors.text), label: "Favourite"),
    BottomNavigationBarItem(icon: SvgPicture.asset("assets/profile.svg", color: (index == 3)? colors.accent:colors.text), label: "Profile"),
    ],
    ),
    );
  }
}

