import 'package:flutter/material.dart';
import 'package:session_0/domain/controllers/password_controller.dart';
import 'package:session_0/domain/forgot_use_case.dart';
import 'package:session_0/domain/sign_in_use_case.dart';
import 'package:session_0/domain/sign_up_use_case.dart';
import 'package:session_0/domain/utils.dart';
import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/home.dart';
import 'package:session_0/presentation/pages/otp.dart';
import 'package:session_0/presentation/pages/sign_in.dart';
import 'package:session_0/presentation/pages/sign_up.dart';
import 'package:session_0/presentation/utils.dart';
import 'package:session_0/presentation/widgets/text_field.dart';

class ForgotPass extends StatefulWidget {
  const ForgotPass({super.key});

  @override
  State<ForgotPass> createState() => _MapPageState();
}

class _MapPageState extends State<ForgotPass> {
  TextEditingController email = TextEditingController();

  ForgotUseCase useCase = ForgotUseCase();

  bool isRedEmail = false;
  bool isValid = false;


  void onChange(_){
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail;
    });
  }






  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
        backgroundColor: colors.background,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 83,),
                Text(
                  "Восстановления пароля",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(height: 8,),
                Text("Введите свою почту",
                  style: Theme.of(context).textTheme.titleMedium,),
                SizedBox(height: 28,),
                CustomField(label: "Почта", hint: "***********@mail.com", controller: email, isValid: !isRedEmail, onChange: onChange,),
                SizedBox(height: 503,),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 46,
                    width: 342,
                    child: FilledButton(
                      onPressed: (isValid) ? (){
                        showLoading(context);
                        useCase.pressButtonForgot(email.text,
                                (_){
                              hideLoading(context);
                              Navigator.push(context, MaterialPageRoute(builder: (context) => OTP(email: email.text))).then((value) => setState(() {

                              }));

                            },
                                (error) async {
                              hideLoading(context);
                              await showError(context, error);
                            }

                        );
                      }:null,
                      child: Text(
                        "Отправить код",
                        style: Theme.of(context).textTheme.labelLarge,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 14,),
                Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                      }));
                    },
                    child: RichText(text: TextSpan(
                        children: [
                          TextSpan(
                              text: "Я вспомнил свой пароль!",
                              style: TextStyle(fontSize: 14, color:colors.subText)
                          ),
                          TextSpan(
                              text: "Вернуться",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: colors.accent,
                                  fontWeight: FontWeight.w700
                              )
                          ),
                        ]
                    )),
                  ),
                ),
                const SizedBox(height: 34),


              ],
            ),
          ),
        )
    );
  }



}
