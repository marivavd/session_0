import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:session_0/presentation/pages/sign_in.dart';
import 'package:session_0/presentation/theme/colors.dart';
import 'package:session_0/presentation/theme/theme.dart';
import 'package:session_0/presentation/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';


Future<void> main() async {
  await Supabase.initialize(
    url: 'https://vmvbsgflwabnyuxsxxoa.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InZtdmJzZ2Zsd2Fibnl1eHN4eG9hIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTE0NDgxNzgsImV4cCI6MjAyNzAyNDE3OH0.UM50aZasU0tmp2sa77T8ee2tqq_mYNW7dmHyo8jn6xo',
  );

  runApp(MyApp());
}

// Get a reference your Supabase client
final supabase = Supabase.instance.client;




class MyApp extends StatefulWidget {
  final Widget body;

  MyApp({super.key, Widget? body}) : body = (body != null) ? body : const SignIn();


  bool isLight = true;


  void changeDiffTheme(BuildContext context) {
    isLight = !isLight;
    context.findAncestorStateOfType<MyAppState>()?.onChangeTheme();
  }

  ColorsApp getColors(BuildContext context) {
    return (isLight) ? colorsLights : colorsDark;
  }

  ThemeData getCurrentTheme() {
    return (isLight) ? light : dark;
  }

  static MyApp of(BuildContext context) {
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  @override
  State<MyApp> createState() => MyAppState();
}

class MyAppState extends State<MyApp> {

  final connectivity = Connectivity();
  late StreamSubscription<List<ConnectivityResult>> _connectivitySubscription;
  void onChangeTheme() {
    setState(() {});
  }
  @override
  void initState() {
    super.initState();
    _connectivitySubscription = connectivity.onConnectivityChanged.listen((event) {
      if (event == ConnectivityResult.none){
        showError(context, 'Network crashed');
      }
    });
  }
  @override
  void dispose() {
    super.dispose();
    _connectivitySubscription.cancel();

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: widget.getCurrentTheme(),
      home: widget.body,
    );
  }
}
