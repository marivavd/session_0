import 'package:session_0/data/models/model_news.dart';
import 'package:session_0/data/storage/favourite_news.dart';

bool isFavourite(ModelNews news){
  bool favourite = false;
  for (var val in favouriteNews){
    if (val.id == news.id){
      favourite = true;
    }
  }
  return favourite;
}