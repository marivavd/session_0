String getTimeForFavourites(DateTime timeFav){
  var delta = DateTime.now().difference(timeFav);
  if (delta.inHours < 23){
    return 'Добавлено ${delta.inHours} часа назад';
  }
  if (delta.inDays < 30){
    return 'Добавлено ${delta.inDays} дня назад';
  }
  if (delta.inDays / 30 < 12){
    return 'Добавлено ${ delta.inDays ~/ 30} месяца назад';
  }
  return 'Добавлено ${ delta.inDays / 30 ~/ 12} года назад';

}