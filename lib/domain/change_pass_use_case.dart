import 'package:session_0/data/models/model_auth.dart';
import 'package:session_0/data/repository/requests.dart';
import 'package:session_0/domain/utils.dart';

class ChangePassUseCase{
  Future<void> pressButtonChangePass(
      String password,
      String confirmpassword,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    if (password != confirmpassword){
      onError('Passwords dont match');
    }
    else{
      requestChangePass() async{
        await change_pass(password);
      }
      await requests(requestChangePass, onResponse, onError);
    }



  }
}