import 'package:session_0/data/repository/requests.dart';
import 'package:session_0/domain/utils.dart';

class ForgotUseCase{
  Future<void> pressButtonForgot(
      String email,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    requestForgot() async{
      await send_email(email);
    }
    await requests(requestForgot, onResponse, onError);



  }
}