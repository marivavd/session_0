import 'package:session_0/data/repository/requests.dart';
import 'package:session_0/domain/utils.dart';

class OTPUseCase{
  Future<void> pressButtonOTP(
      String email,
      String code,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    requestOTP() async{
      await verify_OTP(email, code);
    }
    await requests(requestOTP, onResponse, onError);



  }
}