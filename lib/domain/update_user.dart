import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:session_0/data/models/model_profile.dart';
import 'package:session_0/data/repository/requests.dart';



Future<void> prepare_to_update({required ModelProfile profile, required String fullname, required String phone, required String birthday, required BuildContext context})async{
  profile.fullname = fullname;
  profile.phone = phone;
  profile.birthday = birthday;
  check_avatar(profile.avatar);
  await update_user(profile);
}
void check_avatar(bytes){
  if (bytes != null){
    uploadAvatar(bytes);
  }
  else{
    removeAvatar();
  }
}
