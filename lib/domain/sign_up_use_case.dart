import 'package:session_0/data/models/model_auth.dart';
import 'package:session_0/data/repository/requests.dart';
import 'package:session_0/domain/utils.dart';

class SignUpUseCase{
  Future<void> pressButtonSignUp(
      String email,
      String password,
      String confirmpassword,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    if (password != confirmpassword){
      onError('Passwords dont match');
    }
    else{
      requestSignUp() async{
        await signUp(ModelAuth(email: email, password: password));
      }
      await requests(requestSignUp, onResponse, onError);
    }



}
}