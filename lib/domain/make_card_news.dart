import 'package:session_0/data/models/model_news.dart';
import 'package:session_0/data/models/model_plaform.dart';
import 'package:session_0/data/repository/requests.dart';

Future<List<ModelNews>> makeCardNews() async {
  final data = await get_new_with_coords();
  Map<String, String> sl_months = {'01': 'янв', '02':'фев', '03': 'мар', '04': 'апр', '05': 'май', '06': 'июн', '07': 'июл', '08': 'авг', '09': 'сен', '10': 'окт', '11': 'ноя', '12': 'дек'};
  List<ModelNews> result = [];
  for (int i = 0; i < data.length; i++){
    final mapPlatform = await find_platform(data[i]["id_platform"]);
    String published_at = data[i]['published_at'].toString();
    ModelPlatform platform = ModelPlatform(id: mapPlatform['id'], icon: mapPlatform["icon"], title: mapPlatform['title'], channels: mapPlatform['default_channels'], availableChannels: mapPlatform['allow_channels']);
    result.add(ModelNews(id: data[i]['id'], title: data[i]['title'], text: data[i]['text'], media: data[i]['media'], published: '${published_at.substring(8, 10)} ${sl_months[published_at.substring(5, 7)]}, ${published_at.substring(11, 16)}', platform: platform, type_media: data[i]['type_media']));
  }
  return result;
}

Future<List<ModelNews>> makeNewsHome() async {
  final platforms = loadPlatforms();
  List<String> idPlatforms = [];
  for (int i = 0; i < platforms.length; i ++){
    idPlatforms.add(platforms[i].id);
  }
  final data = await get_news(idPlatforms);
  Map<String, String> sl_months = {'01': 'янв', '02':'фев', '03': 'мар', '04': 'апр', '05': 'май', '06': 'июн', '07': 'июл', '08': 'авг', '09': 'сен', '10': 'окт', '11': 'ноя', '12': 'дек'};
  List<ModelNews> result = [];
  for (int i = 0; i < data.length; i++){
    final mapPlatform = await find_platform(data[i]["id_platform"]);
    String published_at = data[i]['published_at'].toString();
    ModelPlatform platform = ModelPlatform(id: mapPlatform['id'], icon: mapPlatform["icon"], title: mapPlatform['title'], channels: mapPlatform['default_channels'], availableChannels: mapPlatform['allow_channels']);
    result.add(ModelNews(id: data[i]['id'], title: data[i]['title'], text: data[i]['text'], media: data[i]['media'], published: '${published_at.substring(8, 10)} ${sl_months[published_at.substring(5, 7)]}, ${published_at.substring(11, 16)}', platform: platform, type_media: data[i]['type_media']));
  }
  return result;
}