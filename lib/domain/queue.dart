import 'package:session_0/data/models/model_news.dart';
import 'package:session_0/data/storage/favourite_news.dart';


ModelNews push(ModelNews element){
  element.favouriteTime = DateTime.now();
  final sp0 = favouriteNews;
  for (int i = 0; i < favouriteNews.length; i++){
    if (i == 0){
      favouriteNews = [element] + favouriteNews;
    }
  }
  final sp = favouriteNews;

  return element;
}

void reset_queue(){
  favouriteNews.clear();
  for (var val in favouriteNews){
    favouriteNews.add(val);
  }
}

ModelNews remove(ModelNews element){
  favouriteNews.remove(element);
  return element;
}

ModelNews getCurrentElement(){
  return favouriteNews[0];
}
ModelNews getElementByIndex(int index){
  return favouriteNews[index];
}

int len(){
  return favouriteNews.length;
}

List<ModelNews> getListNews(){
  return favouriteNews;
}


