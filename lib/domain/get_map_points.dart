import 'package:session_0/data/models/map_point.dart';
import 'package:session_0/data/repository/requests.dart';

Future<List<MapPoint>> gatMapPoints()async{
  final data = await get_new_with_coords();
  List<MapPoint> result = [];
  for (int i =0; i < data.length; i++){
    result.add(MapPoint(name: data[i]["id"], latitude: double.tryParse(data[i]["geo_lat"].toString())??0.0, longitude: double.tryParse(data[i]["geo_long"].toString())??0.0));
  }
  return result;
}