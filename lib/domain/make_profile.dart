import 'package:flutter/widgets.dart';
import 'package:session_0/data/models/model_profile.dart';
import 'package:session_0/data/repository/requests.dart';

Future<ModelProfile> makeprofile() async {
  Map<String, dynamic> metadata = get_profile_atributes();
  return ModelProfile(fullname: metadata['fullname'], phone: metadata["phone"], birthday: metadata["birthday"], avatar: await getMyAvatar());
}