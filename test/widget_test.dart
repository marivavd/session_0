// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:session_0/data/models/model_news.dart';
import 'package:session_0/data/models/model_plaform.dart';
import 'package:session_0/data/storage/favourite_news.dart';
import 'package:session_0/domain/queue.dart';

import 'package:session_0/main.dart';
import 'package:session_0/presentation/pages/article_page.dart';
import 'package:session_0/presentation/pages/favourite.dart';

void main() {
  group("", () {

    test('−	при удалении любой новости из избранного и ее последующем добавлении она оказывается в начале списка; ', ()
    {
      List<ModelNews> favouriteNewsCopy = [];
      for (var val in favouriteNews){
        favouriteNewsCopy.add(val);
      }
      favouriteNewsCopy.shuffle();
      for (var val in favouriteNewsCopy){
        ModelNews currentElement = remove(val);
        push(currentElement);
        expect(getCurrentElement(), val);
      }


    });
    // test("проверить правильность порядка расположения элементов в списке избранных новостей;", ()  {
    //   for (int i = 0; i < favouriteNews.length; i++){
    //     var favorite = favouriteNews[i];
    //     expect(favorite, getElementByIndex(i));
    //   }
    // });
    // testWidgets("проверить правильность порядка расположения элементов в списке избранных новостей;", (widgetTester) async{
    //   await widgetTester.runAsync(() => widgetTester.pumpWidget(MyApp(
    //     body: MaterialApp(
    //       home: Favourite()
    //     ),
    //   )));
    //
    //
    //   expect(find.byKey(Key('In favourite')), findsOneWidget);
    //   await widgetTester.tap(find.byKey(Key('In favourite')));
    //   await widgetTester.pumpAndSettle();
    //   expect(find.byKey(Key('Not in favourite')), findsOneWidget);
    // });
    test("проверьте, что элемент добавляется в начало списка избранных", () {
      var favoriteModelNew = ModelNews(id: '', title: '', text: '', media: '', published: '', platform: ModelPlatform(id: '', icon: '', title: '', channels: '', availableChannels: ''), type_media: '');
      push(favoriteModelNew);
      expect(getCurrentElement(), favoriteModelNew);
    });
    test("−	проверьте, что при удалении случайного элемента списка избранных новостей, список уменьшается на 1 до полной отчистки", () {
      int len_queue = len();
      while (len() != 0){
        remove(getCurrentElement());
        expect(len(), len_queue - 1);
        len_queue = len();
      }
    });
    testWidgets("−	иконка кнопки «Убрать из избранного» меняется на «Добавить в избранное» при нажатии", (widgetTester) async{
      await widgetTester.runAsync(() => widgetTester.pumpWidget(MyApp(
          body: MaterialApp(
            home: ArticlePage(
              news: favouriteNews[0]
            ),
          ),
        )));
      final inFav = find.byKey(Key('In favourite'));
      expect(inFav, findsOneWidget);
      await widgetTester.tap(inFav);
      await widgetTester.pumpAndSettle();
      expect(find.byKey(Key('Not in favourite')), findsOneWidget);
    });
    testWidgets("−	реализуйте тест на удаление элементов с помощью кнопки «Удалить» до полной отчистки списка", (widgetTester) async{
      await widgetTester.runAsync(() => widgetTester.pumpWidget(MyApp(
        body: MaterialApp(
          home: Favourite(
          ),
        ),
      )));
      expect(find.byKey(Key('Delete')), findsOneWidget);
      await widgetTester.tap(find.byKey(Key('Delete')));


      await widgetTester.pumpAndSettle();
      while (find.byKey(Key("Delete")) == true){

      }
      print(len());
      expect(len(), 0);
    });









  });

}
